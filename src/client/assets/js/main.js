// Progressive Enhancement (feature detection)
if(navigator.serviceWorker) {
  // Service Worker supported

  // Register SW
  navigator.serviceWorker
    .register('/sw.js')
    .then(registration => {
      // const
      //   // Server public key
      //   // NOTE: client should request this from the server in a production
      //   // application
      //   pubKey = 'BDEd6urydfJRq6AS_56qWt6ag9Y4iFiRcgq5tnSKIH15c_-3dXeHXSYRdFDQNryaLlNj6mFD2KApBVM94ScoA5k'
      //
      //   urlBase64ToUint8Array = (base64String) => {
      //     const
      //       padding = '='.repeat((4 - base64String.length % 4) % 4),
      //       base64 = (base64String + padding)
      //         .replace(/-/g, '+')
      //         .replace(/_/g, '/'),
      //       rawData = window.atob(base64),
      //       outputArray = new Uint8Array(rawData.length)
      //
      //     for (let i = 0; i < rawData.length; ++i) {
      //       outputArray[i] = rawData.charCodeAt(i)
      //     }
      //
      //     return outputArray
      //   }
      //
      // registration.pushManager.getSubscription()
      //   .then(sub => {
      //     if(sub) {
      //       return sub
      //     }
      //
      //     const
      //       applicationServerKey = urlBase64ToUint8Array(pubKey)
      //
      //     // Subscription not found, create new one
      //     return registration.pushManager.subscribe({
      //       'userVisibleOnly': true,
      //       applicationServerKey
      //     })
      //   })
      //   .then(sub => sub.toJSON())
      //   // .then(console.log)
      //   .catch(console.error)

//       // registration.onupdatefound = () => {
//       //   const
//       //     newSW = registration.installing
//       //
//       // //   console.log('New SW found', newSW)
//       // //
//       // //   newSW.onstatechange = () => {
//       // //     console.log('newSW:', newSW.state)
//       // //   }
//       //
//       //   if(confirm('Update found. apply now?')) {
//       //     if(registration.active) {
//       //       newSW.postMessage('UPDATE_SELF')
//       //     }
//       //   }
//       // }
//
//       if(registration.active) {
//         registration.active.postMessage('Hello from main.js')
//       }
    })
    .catch(console.error)

//   // Listen to SW messages
//   navigator.serviceWorker.addEventListener('message', e => {
//     console.log('Message from SW:', e.data)
//   })
}


// // Get camera feed
// fetch('/camera-feed.html')
//   .then(res => res.text())
//   .then(html => document.getElementById('camera').innerHTML = html)


// Notification support
// if(window.Notification) {
//   // const
//   //   showNotification = () => {
//   //     const
//   //       opts = {
//   //         'body': 'information goes here',
//   //         'icon': '/assets/img/icon.png'
//   //       },
//   //
//   //       n = new Notification('here this!', opts)
//   //
//   //     // WARNING: following did NOT work
//   //     n.onclick = () => {
//   //       console.log('Notification clicked!!!')
//   //     }
//   //   }
//
//   // Manage permission
//   if('granted' === Notification.permission) {
//     // showNotification()
//   }
//   else if('denied' !== Notification.permission) {
//     // Request permission
//     Notification.requestPermission(permission => {
//       if('granted' === permission) {
//         // showNotification()
//       }
//     })
//   }
// }


// Cache support
//
// NOTE: this feature detection is not necessary within a service worker, as
// every browser with SW support is guaranteed to implement cache support as
// well.
// if(window.caches) {
//   caches
//     // open new cache storage or create new one if does not exists
//     .open('app-v1.0')
//
//     // List all caches available
//     // .keys()
//
//     // Check if specific cache available
//     // .has('test-1')
//
//     // Remove existing cache
//     // .delete('test-1')
//
//     .then(cache => cache
//       // .add('/index.html')
//
//       // WARNING: if one resource failed to load, this operation will NOT cache
//       // ANY of the resources in the list.
//       // .addAll([
//       //   '/index.html',
//       //   '/assets/css/style.css',
//       //   '/assets/js/main.js'
//       // ])
//
//       // Removing a single item from the cache
//       // .delete('/assets/css/style.css')
//
//       // Add resource to cache without fetching
//       // .put('/index.html', new Response('Custom HTML'))
//
//       // Retrieve item from cache
//       // .match('/index.html')
//       // .then(res => res.text())
//
//       // Check cache items
//       .keys()
//     )
//     .then(console.log)
//     .catch(console.error)
// }
